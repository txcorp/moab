#ifndef MOAB_WIN32_CONFIG_H
#define MOAB_WIN32_CONFIG_H

#ifdef WIN32

// Error in repo? Will resolve with MOAB team. Not sure what this is.
// #include "MOAB_export.h"
// temp ... for test
#define MOAB_EXPORT

#define _USE_MATH_DEFINES // for C++  
#include <cmath> 

#else
#define MOAB_EXPORT
#endif

#endif
